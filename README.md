# Reddit PushShift API

An API to query the PushShift Reddit API, supporting both submissions and comments.

PushShift allows you to view delete submissions and comments.

## Usage

Example to fetch the latest `5` submissions from the `r/pics` subreddit:

```ts
import { getSubmission, SortMethod, SortType, SubmissionFields } from 'pushshift-api';

(async () => {
    const response = await getSubmission({
        subreddit: "pics",
        size: 5,
        fields: [
            SubmissionFields.Author,
            SubmissionFields.Title,
            SubmissionFields.Url
        ],
        sort_type: SortType.Created,
        sort: SortMethod.Descending
    });
    if (response) {
        for (let submission of response) {
            console.log(submission);
            /*
                {
                    author: 'An author',
                    title: 'A title',
                    url: 'URL to image'
                }
            */
        }
    }
})();
```
