import fetch from 'node-fetch';

const API_HOST = `https://api.pushshift.io`, REDDIT_API = `${API_HOST}/reddit/search`;

/**
 * Comment fields
 */
export enum CommentFields {
    Author = "author",
    AuthorCreatedUtc = "author_created_utc",
    AuthorFlairCssClass = "author_flair_css_class",
    AuthorFlairText = "author_flair_text",
    AuthorFullname = "author_fullname",
    Body = "body",
    Controversiality = "controversiality",
    CreatedUtc = "created_utc",
    Distinguished = "distinguished",
    Gilded = "gilded",
    Id = "id",
    LinkId = "link_id",
    NestLevel = "nest_level",
    ParentId = "parent_id",
    RetrievedOn = "retrieved_on",
    Score = "score",
    ScoreHidden = "score_hidden",
    Subreddit = "subreddit",
    SubredditId = "subreddit_id"
}

/**
 * Submission fields
 */
export enum SubmissionFields {
    AllAwardings = "all_awardings",
    AllowLiveComments = "allow_live_comments",
    Author = "author",
    AuthorFlairCssClass = "author_flair_css_class",
    AuthorFlairRichtext = "author_flair_richtext",
    AuthorFlairText = "author_flair_text",
    AuthorFlairType = "author_flair_type",
    AuthorFullname = "author_fullname",
    AuthorIsBlocked = "author_is_blocked",
    AuthorPatreonFlair = "author_patreon_flair",
    AuthorPremium = "author_premium",
    Awarders = "awarders",
    CanModPost = "can_mod_post",
    ContentCategories = "content_categories",
    ContestMode = "contest_mode",
    CreatedUtc = "created_utc",
    Domain = "domain",
    FullLink = "full_link",
    Gildings = "gildings",
    Id = "id",
    IsCreatedFromAdsUi = "is_created_from_ads_ui",
    IsCrosspostable = "is_crosspostable",
    IsMeta = "is_meta",
    IsOriginalContent = "is_original_content",
    IsRedditMediaDomain = "is_reddit_media_domain",
    IsRobotIndexable = "is_robot_indexable",
    IsSelf = "is_self",
    IsVideo = "is_video",
    LinkFlairBackgroundColor = "link_flair_background_color",
    LinkFlairRichtext = "link_flair_richtext",
    LinkFlairTextColor = "link_flair_text_color",
    LinkFlairType = "link_flair_type",
    Locked = "locked",
    MediaOnly = "media_only",
    NoFollow = "no_follow",
    NumComments = "num_comments",
    NumCrossposts = "num_crossposts",
    Over_18 = "over_18",
    ParentWhitelistStatus = "parent_whitelist_status",
    Permalink = "permalink",
    Pinned = "pinned",
    PostHint = "post_hint",
    Preview = "preview",
    Pwls = "pwls",
    RetrievedOn = "retrieved_on",
    Score = "score",
    Selftext = "selftext",
    SendReplies = "send_replies",
    Spoiler = "spoiler",
    Stickied = "stickied",
    Subreddit = "subreddit",
    SubredditId = "subreddit_id",
    SubredditSubscribers = "subreddit_subscribers",
    SubredditType = "subreddit_type",
    Thumbnail = "thumbnail",
    ThumbnailHeight = "thumbnail_height",
    ThumbnailWidth = "thumbnail_width",
    Title = "title",
    TotalAwardsReceived = "total_awards_received",
    TreatmentTags = "treatment_tags",
    UpvoteRatio = "upvote_ratio",
    Url = "url",
    UrlOverriddenByDest = "url_overridden_by_dest",
    WhitelistStatus = "whitelist_status",
    Wls = "wls"
}

/**
 * Result sort method
 */
export enum SortMethod {
    Descending = "desc",
    Ascending = "asc"
}

/**
 * Sorting types
 */
export enum SortType {
    Score = "score",
    NumComments = "num_comments", 
    Created = "created_utc"
}

/**
 * A value that can be negated
 */
export interface NegatableValue<T> {
    /**
     * The value
     */
    value: T;
    /**
     * If the value is negated or not
     */
    negated?: boolean;
}

/**
 * Comment search data
 */
export interface SearchOptions {
    /**
     * Comma-delimited base36 ids
     */
    ids?: string[];
    /**
     * Specific search term, searches
     * all possible fields
     */
    q?: string | NegatableValue<string>;
    /**
     * Number of results to return, defaults to 25
     */
    size?: number;
    /**
     * How the results are sorted
     */
    sort?: SortMethod;
    /**
     * Type to sort by
     */
    sort_type?: SortType;
     /**
     * Restrict to a specific author
     */
    author?: string;
    /**
     * Restrict to a specific subreddit
     */
    subreddit?: string;
    /**
     * Return results after this date (Epoch value or integer)
     */
    after?: string;
    /**
     * Return results before this date
     */
    before?: string;
}

export interface CommentSearchOptions extends SearchOptions {
    /**
     * Fields to include in the search result
     */
    fields?: CommentFields[];
}

/**
 * Submission search data
 */
export interface SubmissionSearchOptions extends SearchOptions {
    /**
     * Only search the title
     */
    title?: string | NegatableValue<string>;
    /**
     * Only search the self text
     */
    selftext?: string | NegatableValue<string>;
    /**
     * Fields to include in the search result
     */
    fields?: SubmissionFields[];
    /**
     * Restrict results based on score
     */
    score?: number;
    /**
     * Restrict results based on number of comments
     */
    num_comments?: number;
    /**
     * Restrict to nsfw or sfw content
     */
    over_18?: boolean;
    /**
     * Restrict to video content
     */
    is_video?: boolean;
    /**
     * Restrict to locked or unlocked content
     */
    locked?: boolean;
    /**
     * Restrict to sticiked or non stickied content
     */
    stickied?: boolean;
    /**
     * Restrict to spoiler or non spoiler content
     */
    spoiler?: boolean;
    /**
     * Restrict to content mode submissions
     */
    contest_mode?: boolean;
}

/**
 * A submission object
 */
export interface Submission {
    all_awardings?: object[];
    allow_live_comments?: boolean;
    author?: string;
    author_flair_css_class?: object;
    author_flair_richtext?: object[];
    author_flair_text?: object;
    author_flair_type?: string;
    author_fullname?: string;
    author_is_blocked?: boolean;
    author_patreon_flair?: boolean;
    author_premium?: boolean;
    awarders?: object[];
    can_mod_post?: boolean;
    content_categories?: object[];
    contest_mode?: boolean;
    created_utc?: number;
    domain?: string;
    full_link?: string;
    gildings?: object;
    id?: string;
    is_created_from_ads_ui?: boolean;
    is_crosspostable?: boolean;
    is_meta?: boolean;
    is_original_content?: boolean;
    is_reddit_media_domain?: boolean;
    is_robot_indexable?: boolean;
    is_self?: boolean;
    is_video?: boolean;
    link_flair_background_color?: string;
    link_flair_richtext?: object[];
    link_flair_text_color?: string;
    link_flair_type?: string;
    locked?: boolean;
    media_only?: boolean;
    no_follow?: boolean;
    num_comments?: number;
    num_crossposts?: number;
    over_18?: boolean;
    parent_whitelist_status?: string;
    permalink?: string;
    pinned?: boolean;
    post_hint?: string;
    preview?: object;
    pwls?: number;
    retrieved_on?: number;
    score?: number;
    selftext?: string;
    send_replies?: boolean;
    spoiler?: boolean;
    stickied?: boolean;
    subreddit?: string;
    subreddit_id?: string;
    subreddit_subscribers?: number;
    subreddit_type?: string;
    thumbnail?: string;
    thumbnail_height?: number;
    thumbnail_width?: number;
    title?: string;
    total_awards_received?: number;
    treatment_tags?: object[];
    upvote_ratio?: number;
    url?: string;
    url_overridden_by_dest?: string;
    whitelist_status?: string;
    wls?: number;
}

/**
 * Comment object
 */
export interface Comment {
    author?: string;
    author_created_utc?: number;
    author_flair_css_class?: object;
    author_flair_text?: object;
    author_fullname?: string;
    body?: string;
    controversiality?: number;
    created_utc?: number;
    distinguished?: object;
    gilded?: number;
    id?: string;
    link_id?: string;
    nest_level?: number;
    parent_id?: string;
    retrieved_on?: number;
    score?: number;
    score_hidden?: boolean;
    subreddit?: string;
    subreddit_id?: string;
}

/**
 * A query builder which does not encode values or keys
 */
export class QueryBuilder {

    private query: string;

    constructor(url: string) {
        this.query = `${url}?`;
    }

    append(key: string, value: any) {
        if (!this.query.endsWith("?"))
            this.query += "&";
        this.query += `${key}=${value}`;
        return this;
    }

    toString() {
        return this.query;
    }

    /**
     * Appends all options to this query
     * 
     * @param options Search options
     * @returns this
     */
    addOptions(options: SearchOptions) {
        const record = <Record<string, any>> options;
        for (let key of Object.keys(record)) {
            if (record[key] instanceof Object && 'value' in record[key]) {
                const negatableValue = <NegatableValue<any>> record[key];
                this.append(key + (negatableValue.negated === true ? ":not" : ""), negatableValue.value);
            } else if (Array.isArray(record[key])) {
                this.append(key, (<Array<any>> record[key]).join(","));
            } else {
                this.append(key, record[key]);
            }
        }
        return this;
    }

}

/**
 * Queries an endpoint
 * 
 * @param endpoint The endpoint to query
 * @param options Search options
 * @returns The quried data
 */
export async function query<T>(endpoint: string, options: SearchOptions) {
    const query = new QueryBuilder(`${REDDIT_API}/${endpoint}`);
    try {
        const response = await fetch(query.addOptions(options).toString());
        const json = await response.json();
        return <T[]> json.data;
    } catch (ex) {
        console.error("Unable to fetch data", ex);
    }
}

/**
 * Fetches all submissions that matches the search options
 * 
 * @param options Submission search options
 * @returns An array of submissions
 */
export async function getSubmission(options: SubmissionSearchOptions) {
    return await query<Submission>("submission", options);
}

/**
 * Fetches all comments matching the search options
 * 
 * @param options Comment search options
 * @returns An array of comments
 */
export async function getComment(options: CommentSearchOptions) {
    return await query<Comment>("comment", options);
}
